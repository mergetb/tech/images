#!/bin/bash

set -e
set -x

curl \
    -L https://gitlab.com/mergetb/facility/mars/-/jobs/artifacts/main/raw/build/mars-install?job=make \
    -o /tmp/mars-install

###
# Mars install service

cat <<EOF > /tmp/mars-install.service
[Unit]
Description=Mars Install Service
After=network-online.target
Wants=network-online.target

[Service]
ExecStart=/usr/local/bin/mars-install
Type=simple
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF

sudo mv /tmp/mars-install /usr/local/bin/mars-install
sudo mv /tmp/mars-install.service /etc/systemd/system/mars-install.service

sudo chmod 755 /usr/local/bin/mars-install
sudo chmod 644 /etc/systemd/system/mars-install.service

sudo systemctl enable mars-install.service
