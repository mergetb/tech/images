#!/bin/bash

set -e
set -x

# disable NetworkManager
sudo systemctl disable NetworkManager.service

sudo systemctl enable systemd-networkd
sudo systemctl enable systemd-resolved
