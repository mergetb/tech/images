#!/bin/bash

set -e
set -x

sudo tee /etc/selinux/config <<EOF
SELINUX=permissive
SELINUXTYPE=targeted
EOF

# for the rest of the install process
sudo setenforce permissive
