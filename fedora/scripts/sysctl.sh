#!/bin/bash

set -e
set -x

# Always clear page cache before swapping, which can lead to VM crashes
sudo tee /etc/sysctl.d/90-mars-vm.conf <<EOF
vm.swappiness=0
vm.vfs_cache_pressure=150
EOF
