#!/bin/bash

set -e
set -x

# add NVME modules and re-generate initramfs
sudo tee -a /etc/dracut.conf.d/nvme.conf <<EOF
add_drivers+=" nvme_core nvme "
EOF

sudo dracut -f
