#!/bin/bash

set -e
set -x

sudo tee -a /etc/dnf/dnf.conf <<EOF
max_parallel_downloads=10
fastestmirror=True
EOF

sudo dnf update -y
sudo dnf install -y \
    tmux \
    vim \
    grub2-efi \
    grub2-efi-modules \
    grub2-tools \
    shim \
    socat \
    podman \
    podman-remote

sudo dnf clean all
