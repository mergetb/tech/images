#!/bin/bash

set -x

# Disable autobooting on startup
# echo 'echo "booting" > /etc/sysconfig/noautologin' | sudo tee -a /opt/bootsync.sh

# Generate a new machine ID on startup
sudo rm -rf /etc/machine-id
echo "dbus-uuidgen --ensure=/etc/machine-id" | sudo tee -a /opt/bootlocal.sh

# Move /opt to persistent /mnt/pfs
sudo cp -r /opt /mnt/pfs/

# Rename things in /tmp/collect
sudo mv /tmp/collect/corepure64.gz /tmp/collect/sled-initramfs
sudo mv /tmp/collect/vmlinuz64 /tmp/collect/sled-kernel

# Clean home
sudo rm -rf /home/tc/*

# Clear bash history
history -c
sudo history -c

sudo filetool.sh -b || true

# Zero out pfs
sudo dd if=/dev/zero of=/mnt/pfs/EMPTY bs=1M || :
sudo rm /mnt/pfs/EMPTY
