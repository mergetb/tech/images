#!/bin/sh

set -x

# Install stuff for grub and formatting
until tce-load -wil e2fsprogs popt grub2-multi squashfs-tools util-linux bash advcomp ; do sleep 5 ; done

# Format /dev/vda
#   vda1: regular linux partition
DISK=$(lsblk | grep ' disk $' | awk '{ if($3 == "0") print $1 }')
sudo fdisk /dev/$DISK << EOF
n
p
1


w
EOF

# Format and mount vda1
sudo mkfs.ext4 /dev/"$DISK"1
echo "y" | sudo tune2fs /dev/"$DISK"1 -U "a0000000-0000-0000-0000-00000000000a"
sudo mkdir -p /mnt/pfs
sudo mount /dev/"$DISK"1 /mnt/pfs

# Set backup directory
echo "$DISK"1/tce | sudo tee /etc/sysconfig/backup_device
