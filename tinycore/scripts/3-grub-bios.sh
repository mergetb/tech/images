#!/bin/bash

set -ex

# Copy over boot files
sudo mkdir -p /mnt/pfs/boot
sudo cp /tmp/collect/* /mnt/pfs/boot || true

# Install grub
DISK=$(lsblk | grep ' disk $' | awk '{ if($3 == "0") print $1 }')
sudo /usr/local/sbin/grub-install --target=i386-pc --boot-directory=/mnt/pfs /dev/$DISK

# Make grub config
sudo touch /mnt/pfs/grub/grub.cfg

#UUID=$(blkid -s UUID /dev/"$DISK"1 | grep -o \".*\" | cut -d '"' -f2)
UUID="a0000000-0000-0000-0000-00000000000a"
sudo -E tee /mnt/pfs/grub/grub.cfg <<EOF
#search --no-floppy --fs-uuid --set=root $UUID

loadfont "unicode"
set gfxmode=text
set gfxpayload=keep
set timeout=2

menuentry "corepure64" {
linux /boot/vmlinuz64 quiet text tce=UUID="$UUID" waitusb=10:UUID="$UUID" showapps opt=UUID="$UUID" home=UUID="$UUID" root=UUID="$UUID" rootfstype=ext4 rw net.ifnames=0 biosdevname=0 console=tty0 console=ttyS0,115200n8 earlyprintk=ttyS0
initrd /boot/corepure64.gz
}
EOF
