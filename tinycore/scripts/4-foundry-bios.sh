#!/bin/bash

sudo mkdir -p /tmp/foundryc/usr/local/bin
sudo mkdir -p /tmp/foundryc/usr/local/etc/init.d

sudo curl \
    -L https://gitlab.com/mergetb/tech/foundry/-/jobs/artifacts/main/raw/build/foundryc?job=make \
    -o /tmp/foundryc/usr/local/bin/foundryc

sudo chmod 755 /tmp/foundryc/usr/local/bin/foundryc

sudo -E tee /tmp/foundryc/usr/local/etc/init.d/foundryc << 'EOF'
#!/bin/bash

exec > >(tee -i /var/log/foundryc.log)

# foundryc start script
[ "$(id -u)" = 0 ] || { echo "must be root" ; exit 1; }

COUNT=0
TIMES=100

EMBIGGEN_PATH=`sudo lsblk -b -o MOUNTPOINT,UUID | awk '$2 ~ /a0000000-0000-0000-0000-00000000000a/ {print $1}'`

BOOT_PART=`sudo lsblk -b -o PATH,UUID | awk '$2 ~ /a0000000-0000-0000-0000-00000000000a/ {print $1}'`
BOOT_PART=`basename ${BOOT_PART}`

GRUB_CFG="/mnt/"$BOOT_PART"/grub/grub.cfg"

sudo ln -s /usr/local/sbin/sfdisk /sbin/sfdisk

echo "Starting foundryc..."

echo /usr/local/bin/foundryc -no-systemd -embiggen-path ${EMBIGGEN_PATH} -grub-cfg ${GRUB_CFG}
until /usr/local/bin/foundryc -no-systemd -embiggen-path ${EMBIGGEN_PATH} -grub-cfg ${GRUB_CFG} 2>&1
do
    [ $COUNT -ge $TIMES ] && echo "foundryc failed... no more retries" && exit 1
    COUNT=$((COUNT + 1))
    echo
    echo "foundryc failed, retrying $COUNT/$TIMES..."
    echo "=========================================="
    echo
    
    sleep 5
    echo /usr/local/bin/foundryc -no-systemd -embiggen-path ${EMBIGGEN_PATH} -grub-cfg ${GRUB_CFG}
done

echo "foundryc successfully ran!"

EOF

sudo chmod 755 /tmp/foundryc/usr/local/etc/init.d/foundryc

cd /tmp

sudo mksquashfs foundryc foundryc.tcz

echo "echo 172.30.0.1 foundry | sudo tee -a /etc/hosts" | sudo tee -a /opt/bootlocal.sh
echo "sudo /usr/local/etc/init.d/foundryc start &" | sudo tee -a /opt/bootlocal.sh

cp /tmp/foundryc.tcz /etc/sysconfig/tcedir/optional/foundryc.tcz

echo "foundryc.tcz" | sudo tee -a /etc/sysconfig/tcedir/onboot.lst
