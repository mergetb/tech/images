#!/bin/bash

set -x

# Reset installed packages, so installing stuff works again
sudo rm -rf /usr/local/tce.installed/*

# Move /tce directory to persistent /mnt/pfs
sudo mkdir -p /mnt/pfs/tce/optional

sudo rm /etc/sysconfig/tcedir
sudo ln -s /mnt/pfs/tce /etc/sysconfig/tcedir

# Set permissions
sudo chown -R tc:staff /mnt/pfs

# Remaster initramfs
## Mount the fake optical disc for boot parts
CD=$(lsblk | grep ' rom ' | awk 'NR==1{print $1}')
sudo mount /dev/$CD

## Copy over boot files
sudo mkdir -p /tmp/collect
sudo cp /mnt/$CD/boot/* /tmp/collect || true

## Make directory
sudo mkdir -p /tmp/extract
pushd /tmp/extract

## Extract corepure64.gz
zcat /tmp/collect/corepure64.gz | sudo cpio -i -H newc -d

## Edits:
### Enable serial console on tty0 and ttyS0
sudo sed 's|\(tty1.*\)|tty0::respawn:/sbin/getty 38400 tty0\nttyS0::respawn:/sbin/getty 115200 ttyS0\n\1|g' -i etc/inittab
### Wait for drive, no DHCP
sudo sed -i '1s;^;NODHCP=1\nWAITUSB="10:UUID=a0000000-0000-0000-0000-00000000000a"\n;' etc/init.d/tc-config

## Recompress
sudo find | sudo cpio -o -H newc | sudo su root -c '(gzip -2 > /tmp/collect/corepure64.gz)'
sudo advdef -z /tmp/collect/corepure64.gz

popd

# Persistently install packages
tce-load -wic ca-certificates openssh nano curl dbus iproute2 e2fsprogs util-linux gdisk popt dosfstools bash

# Start ssh

cd /usr/local/etc/ssh
sudo cp ssh_config.orig ssh_config
sudo cp sshd_config.orig sshd_config

sudo tee -a sshd_config <<EOF

UseDNS no
EOF

sudo /usr/local/etc/init.d/openssh start
sudo tee -a /opt/bootlocal.sh <<EOF
sudo /usr/local/etc/init.d/openssh start &
sudo /usr/local/tce.installed/ca-certificates &
sudo sysctl
EOF

# Misc

# sysctl

sudo touch /etc/sysctl.conf
sudo mkdir -p /etc/sysctl.d/
sudo ln -s /etc/sysctl.conf /etc/sysctl.d/sysctl.conf

# Disable DHCP on startup by replacing it with an empty script
echo "" | sudo tee /etc/init.d/dhcp.sh

# Add group sudo
sudo addgroup sudo

# Add sudo group to sudoers
echo "%sudo ALL=(ALL:ALL) NOPASSWD:ALL" | sudo tee -a /etc/sudoers

# Add users
sudo adduser test -s /bin/bash << EOF
test
test
EOF

# Add test to sudoers
echo "test ALL=NOPASSWD: ALL" | sudo tee -a /etc/sudoers

# Add test to group sudo
sudo addgroup test sudo

sudo passwd tc << EOF
vmuser#123
vmuser#123
EOF

# Tell TC to save things
#   etc/passwd - user identities
#   etc/shadow - user passwords
#   usr/local/etc/ssh - ssh config files and certs
#   usr/local/etc/init.d - ssh startup script

sudo tee /opt/.filetool.lst <<EOF
opt
home
etc/machine-id
etc/passwd
etc/shadow
etc/group
etc/sudoers
etc/sysctl.conf
etc/sysctl.d/sysctl.conf
etc/init.d/dhcp.sh
usr/local/etc/ssh
usr/local/etc/init.d
EOF

sudo filetool.sh -b || true
