#!/bin/bash

set -ex

# Format and mount the EFI partition
DISK=$(lsblk | grep ' disk $' | awk '{ if($3 == "0") print $1 }')
sudo /usr/local/sbin/mkfs.fat -F 32 /dev/"$DISK"1
sudo mkdir -p /mnt/efs
sudo mount /dev/"$DISK"1 /mnt/efs

# Copy over boot files
sudo mkdir -p /mnt/efs/boot
sudo cp /tmp/collect/* /mnt/efs/boot || true

# Install grub
sudo /usr/local/sbin/grub-install --target=x86_64-efi --boot-directory=/mnt/efs/EFI/BOOT --efi-directory=/mnt/efs --removable

# Make grub config
sudo touch /mnt/efs/EFI/BOOT/grub/grub.cfg

#UUID=$(blkid -s UUID /dev/"$DISK"2 | grep -o \".*\" | cut -d '"' -f2)
UUID="a0000000-0000-0000-0000-00000000000a"
sudo -E tee /mnt/efs/EFI/BOOT/grub/grub.cfg <<EOF
#search --no-floppy --fs-uuid --set=root $UUID

loadfont "unicode"
set gfxmode=text
set gfxpayload=keep
set timeout=2

menuentry "corepure64" {
linux /boot/vmlinuz64 quiet text tce=UUID="$UUID" waitusb=10:UUID="$UUID" showapps opt=UUID="$UUID" home=UUID="$UUID" apparmor=0 root=UUID="$UUID" rootfstype=ext4 rw net.ifnames=0 biosdevname=0 console=tty0 console=ttyS0,115200n8 earlyprintk=ttyS0
initrd /boot/corepure64.gz
}
EOF
