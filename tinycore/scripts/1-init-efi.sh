#!/bin/sh

set -x

# Get packages
until tce-load -wil e2fsprogs dosfstools gdisk grub2-multi squashfs-tools util-linux bash advcomp ; do sleep 5 ; done

# Format /dev/sda
#   sda1: EFI boot partition  
#   sda2: regular linux partition
DISK=$(lsblk | grep ' disk $' | awk '{ if($3 == "0") print $1 }')
sudo gdisk /dev/$DISK << EOF
n
1

30M
EF00
n
2



x
c
1
10000000-0000-0000-0000-000000000001
c
2
a0000000-0000-0000-0000-00000000000a
w
y
EOF

# Format and mount sda2
sudo mkfs.ext4 /dev/"$DISK"2
echo "y" | sudo tune2fs /dev/"$DISK"2 -U "a0000000-0000-0000-0000-00000000000a"
sudo mkdir -p /mnt/pfs
sudo mount /dev/"$DISK"2 /mnt/pfs

# Set backup directory
echo "$DISK"2/tce | sudo tee /etc/sysconfig/backup_device
