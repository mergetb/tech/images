all: bin/shrink bin/pack fedora debian ubuntu tinycore

.PHONY: fedora debian ubuntu alpine tinycore clean

fedora: bin/pack bin/shrink
	bin/pack
	make -j `nproc` -C fedora

debian: bin/pack bin/shrink
	bin/pack
	make -j `nproc` -C debian

ubuntu: bin/pack bin/shrink
	bin/pack
	make -j `nproc` -C ubuntu

alpine: bin/pack bin/shrink
	bin/pack
	make -j `nproc` -C alpine

tinycore: bin/pack bin/shrink
	bin/pack
	make -j `nproc` -C tinycore

bin/pack: .tools/libguestfs cmd/pack/*.go
	mkdir -p bin
	go build -o $@ cmd/pack/*.go
	bin/pack

bin/shrink: .tools/libguestfs cmd/shrink/*.go
	mkdir -p bin
	go build -o $@ cmd/shrink/*.go

# The libguestfs-devel packages go not include the golang bindings for some reason.
# So, we unfortuately have to download and pin a version...
.tools/libguestfs:
	mkdir -p .tools
	curl -o .tools/libguestfs.tar.gz https://download.libguestfs.org/1.48-stable/libguestfs-1.48.2.tar.gz
	tar -xvf .tools/libguestfs.tar.gz -C .tools
	rm -rf .tools/libguestfs.tar.gz
	ln -s `pwd`/.tools/libguestfs-* .tools/libguestfs

clean:
	rm -rf bin/pack .tools
	make -C fedora clean
	make -C debian clean
	make -C ubuntu clean
	make -C tinycore clean
