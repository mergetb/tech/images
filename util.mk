define packer
	rm -rf $1
	PACKER_LOG=1 /usr/bin/packer build $<
	sfdisk --part-uuid $1/$2 1 10000000-0000-0000-0000-000000000001 || true
	sfdisk --part-uuid $1/$2 2 A0000000-0000-0000-0000-00000000000A || true
	../bin/shrink $1/$2 -c
	zstd -T0 --no-progress --rm --ultra -22 $1/$2
endef

define packernoshrink
	rm -rf $1
	PACKER_LOG=1 /usr/bin/packer build $<
	sfdisk --part-uuid $1/$2 1 10000000-0000-0000-0000-000000000001 || true
	sfdisk --part-uuid $1/$2 2 A0000000-0000-0000-0000-00000000000A || true
	zstd -T0 --no-progress --rm --ultra -22 $1/$2
endef

define packernouuid
	rm -rf $1
	PACKER_LOG=1 /usr/bin/packer build $<
	../bin/shrink $1/$2 -c
	zstd -T0 --no-progress --rm --ultra -22 $1/$2
endef
