package main

import (
	"fmt"
	"strconv"
	"strings"

	diskfs "github.com/diskfs/go-diskfs"
	log "github.com/sirupsen/logrus"
	"libguestfs.org/guestfs"
)

func shrink_fs(data *ImageData) error {

	disk, err := diskfs.OpenWithMode(data.File, diskfs.ReadOnly)
	if err != nil {
		return err
	}

	part_table_diskfs, err := disk.GetPartitionTable()
	if err != nil {
		return err
	}

	data.PartitionTableType = part_table_diskfs.Type()

	g, err := guestfs.Create()
	if err != nil {
		return err
	}
	defer g.Close()

	// g.Set_trace(true)

	err = g.Add_drive(data.File, nil)
	if err != nil {
		return err
	}

	err = g.Launch()
	if err != nil {
		return err
	}

	defer func() {
		err = g.Shutdown()

		if err != nil {
			log.Error(err)
		}
	}()

	filesystems, err := g.List_filesystems()

	if err != nil {
		return err
	}
	if len(filesystems) < 1 {
		err = fmt.Errorf("guestfs: expecting at least 1 filesystem")
		return err
	}

	// filesystems is a map annoyingly enough, so we have to loop through it
	// to get the last partition aka partition with the biggest number
	for p, t := range filesystems {
		cur, err := g.Part_to_partnum(p)

		if err != nil {
			return err
		}

		if cur > data.PartNum {
			data.PartNum = cur
			data.Part = p
			data.PartFS = t
			data.PartDev, err = g.Part_to_dev(p)

			if err != nil {
				return err
			}
		}
	}

	if !strings.HasPrefix(data.PartFS, "ext") {
		err = fmt.Errorf("guestfs: expecting ext filesytem in final partition: %s:%s", data.Part, data.PartFS)
		return err
	}

	ss_size, err := g.Blockdev_getss(data.PartDev)
	if err != nil {
		return err
	}
	data.PartitionTableSectorSize = int64(ss_size)

	log.Infof("Resizing filesystem...")
	err = g.Mount(data.Part, "/")
	if err != nil {
		return err
	}

	err = g.Fstrim("/", nil)
	if err != nil {
		return err
	}

	err = g.Umount("/", nil)
	if err != nil {
		return err
	}

	err = g.E2fsck_f(data.Part)
	if err != nil {
		return err
	}

	err = g.Resize2fs_M(data.Part)
	if err != nil {
		return err
	}

	tune_output, err := g.Tune2fs_l(data.Part)
	if err != nil {
		return err
	}

	num_blocks_s, ok := tune_output["Block count"]
	if !ok {
		err = fmt.Errorf("guestfs: block count not in tune2fs_l output")
		return err
	}

	num_blocks, err := strconv.Atoi(num_blocks_s)
	if err != nil {
		return err
	}

	log.Debugf("num_blocks: %d", num_blocks)

	block_size_s := tune_output["Block size"]
	if !ok {
		err = fmt.Errorf("guestfs: block size not in tune2fs_l output")
		return err
	}

	block_size, err := strconv.Atoi(block_size_s)
	if err != nil {
		return err
	}

	log.Debugf("block_size: %d", block_size)

	part_table, err := g.Part_list(data.PartDev)
	if err != nil {
		return err
	}

	data.PartStartLoc = int64((*part_table)[data.PartNum-1].Part_start)
	data.PartEndLoc = data.PartStartLoc + int64(num_blocks*block_size) - 1

	// round PartEndLoc to the next multiple of the sector size
	num_sectors := (data.PartEndLoc + data.PartitionTableSectorSize) / data.PartitionTableSectorSize

	// add 33 sectors for gpt (and in general),
	// as gpt also stores the partition table at the end
	if data.PartitionTableType != "dos" {
		num_sectors += 33
	}

	// multiply the sectors by the sector size
	data.NewImageSize = num_sectors * data.PartitionTableSectorSize

	return nil

}
