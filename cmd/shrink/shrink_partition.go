package main

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"

	log "github.com/sirupsen/logrus"
)

func shrink_partition(data *ImageData) error {
	// exec parted because the libguestfs one will not shrink partitions
	log.Debugf(
		"%s %s %s %s %s %s %s %s",
		"parted",
		"---pretend-input-tty",
		data.File,
		"unit",
		"b",
		"resizepart",
		fmt.Sprintf("%d", data.PartNum),
		fmt.Sprintf("%d", data.PartEndLoc),
	)
	cmd := exec.Command(
		"parted",
		"---pretend-input-tty",
		data.File,
		"unit", "b",
		"resizepart",
		fmt.Sprintf("%d", data.PartNum),
		fmt.Sprintf("%d", data.PartEndLoc),
	)

	stdin := bytes.NewReader([]byte("Yes\n"))
	stdout := new(bytes.Buffer)
	stderr := new(bytes.Buffer)

	cmd.Stdin = stdin
	cmd.Stdout = stdout
	cmd.Stderr = stderr

	err := cmd.Run()

	log.Debugf("parted:\n%s", stdout)

	if err != nil {
		return fmt.Errorf("parted:\n  stderr: %s\n  err: %v", stderr, err)
	}

	// truncate file
	err = os.Truncate(data.File, data.NewImageSize)
	if err != nil {
		return err
	}

	// fix secondary partition table if gpt
	if data.PartitionTableType != "dos" {
		log.Info("Running sgdisk...")
		cmd := exec.Command("sgdisk", "-e", data.File)

		stdout := new(bytes.Buffer)
		stderr := new(bytes.Buffer)
		cmd.Stdout = stdout
		cmd.Stderr = stderr

		cmd.Run()

		log.Debugf("sgdisk:\n%s", stdout)

		if err != nil {
			return fmt.Errorf("sgdisk:\n  stderr: %s\n  err: %v", stderr, err)
		}
	}

	return nil
}
