package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"

	log "github.com/sirupsen/logrus"
)

type ImageData struct {
	File                     string
	BackupDirectory          string
	BackupFile               string
	PartitionTableType       string
	PartitionTableSectorSize int64
	Part                     string
	PartFS                   string
	PartDev                  string
	PartNum                  int
	PartStartLoc             int64
	PartEndLoc               int64
	NewImageSize             int64
}

func backup_image(data *ImageData) error {

	var err error

	// Make a temporary folder
	data.BackupDirectory, err = ioutil.TempDir("", "shrink-disk-image-")
	if err != nil {
		return err
	}

	// Put the backup image inside of the temporary folder
	data.BackupFile = path.Join(data.BackupDirectory, path.Base(data.File)+".bak")

	// Golang surpringly does not have an easy copy method.
	// It's better to use cp instead of doing golang streams,
	// in case the file is sparse
	cmd := exec.Command(
		"cp",
		data.File,
		data.BackupFile,
	)

	stdout := new(bytes.Buffer)
	stderr := new(bytes.Buffer)

	cmd.Stdout = stdout
	cmd.Stderr = stderr

	err = cmd.Run()

	if err != nil {
		return fmt.Errorf("cp:\n  stderr: %s\n  err: %v", stderr, err)
	}

	return nil

}

func restore_backup(data *ImageData) error {
	log.Infof("Restoring backup...")
	return os.Rename(data.BackupFile, data.File)
}

func remove_backup(data *ImageData) error {
	log.Infof("Deleting backup...")
	return os.Remove(data.BackupFile)
}

func remove_backup_directory(data *ImageData) error {
	return os.Remove(data.BackupDirectory)
}

func parted_print(data *ImageData, name string) error {
	cmd := exec.Command(
		"parted",
		data.File,
		"unit",
		"b",
		"print",
		"-s",
	)

	stdout := new(bytes.Buffer)
	stderr := new(bytes.Buffer)

	cmd.Stdout = stdout
	cmd.Stderr = stderr

	err := cmd.Run()

	log.Infof("%s:\n%s", name, stdout)

	if err != nil {
		return fmt.Errorf("parted:\n  stderr: %s\n  err: %v", stderr, err)
	}

	return nil
}

func guestmount(file, part, dest string) error {
	cmd := exec.Command(
		"guestmount",
		"-a",
		file,
		"-m",
		part,
		"--ro",
		"-o",
		"allow_other",
		dest,
	)

	stdout := new(bytes.Buffer)
	stderr := new(bytes.Buffer)

	cmd.Stdout = stdout
	cmd.Stderr = stderr

	err := cmd.Run()

	log.Debugf("guestmount:\n%s", stdout)

	if err != nil {
		return fmt.Errorf("guestmount:\n  stderr: %s\n  err: %v", stderr, err)
	}

	return nil
}

func guestunmount(dest string) error {
	cmd := exec.Command(
		"guestunmount",
		dest,
	)

	stdout := new(bytes.Buffer)
	stderr := new(bytes.Buffer)

	cmd.Stdout = stdout
	cmd.Stderr = stderr

	err := cmd.Run()

	log.Debugf("guestunmount:\n%s", stdout)

	if err != nil {
		return fmt.Errorf("guestmount:\n  stderr: %s\n  err: %v", stderr, err)
	}

	return nil

}
