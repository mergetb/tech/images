package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"

	log "github.com/sirupsen/logrus"
	"libguestfs.org/guestfs"
)

func check_partition(data *ImageData) error {

	g, err := guestfs.Create()
	if err != nil {
		return err
	}
	defer g.Close()

	// g.Set_trace(true)

	err = g.Add_drive(data.File, nil)
	if err != nil {
		return err
	}

	err = g.Launch()
	if err != nil {
		return err
	}

	defer func() {
		err = g.Shutdown()

		if err != nil {
			log.Error(err)
		}
	}()

	err = g.E2fsck_f(data.Part)
	if err != nil {
		return err
	}

	log.Infof("e2fsck-f ok!")

	return nil

}

func check_filesystem(data *ImageData) error {
	log.Infof("Checking that the filesystem on %s hasn't gone bad...", data.Part)

	new_tmp, err := ioutil.TempDir("", "shrink-disk-new-")
	if err != nil {
		return err
	}
	defer func() {
		err = os.RemoveAll(new_tmp)
		if err != nil {
			log.Error(err)
		}
	}()

	bak_tmp, err := ioutil.TempDir("", "shrink-disk-bak-")
	if err != nil {
		return err
	}
	defer func() {
		err = os.RemoveAll(bak_tmp)
		if err != nil {
			log.Error(err)
		}
	}()

	err = guestmount(data.File, data.Part, new_tmp)
	if err != nil {
		return err
	}
	defer func() {
		err = guestunmount(new_tmp)
		if err != nil {
			log.Error(err)
		}
	}()

	err = guestmount(data.BackupFile, data.Part, bak_tmp)
	if err != nil {
		return err
	}
	defer func() {
		err = guestunmount(bak_tmp)
		if err != nil {
			log.Error(err)
		}
	}()

	log.Info("Using sudo, you might need to enter a password...")

	cmd := exec.Command(
		"bash",
		"-c",
		fmt.Sprintf("sudo diff -r -q --no-dereference %s %s 2>&1 | grep -v '^File.*is a socket while file.*is a socket$' || true", new_tmp, bak_tmp),
	)

	cmd.Stdin = os.Stdin
	combined := new(bytes.Buffer)

	cmd.Stdin = os.Stdin
	cmd.Stdout = io.MultiWriter(combined, os.Stdout)
	cmd.Stderr = io.MultiWriter(combined, os.Stderr)

	// The only thing we care about is that the combined output is empty
	cmd.Run()
	output := combined.String()
	if output != "" {
		return fmt.Errorf("diff:\n%s", output)
	}

	log.Infof("Filesystem ok!")

	return nil
}
