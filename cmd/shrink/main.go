package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"
)

func main() {
	check_diff := false

	root := &cobra.Command{
		Use:   "shrink <image>",
		Short: "Shrink an image. If an error occurs, the original image will be restored.",
		Args:  cobra.ExactArgs(1),
		Run: func(_ *cobra.Command, args []string) {
			err := shrink_image(args[0], check_diff)

			if err != nil {
				log.Error(err)
				os.Exit(1)
			}
		},
	}

	root.Flags().BoolVarP(
		&check_diff, "check-diff", "c", false,
		"After minimizing the image, check to see if the new image's filesystems differs from the original image. Check README.md for requirements to use this option.")

	root.Execute()
}

func shrink_image(image string, check_diff bool) error {

	data := &ImageData{
		File: image,
	}

	// Make backup
	err := backup_image(data)
	if err != nil {
		return err
	}
	defer func() {
		err = remove_backup_directory(data)
		if err != nil {
			log.Error(err)
		}
	}()

	// On error, restore backup
	defer func() {
		if err != nil {
			err = restore_backup(data)
			if err != nil {
				log.Error(err)
			}
		}
	}()

	// Print parted info
	err = parted_print(data, "Before parted")
	if err != nil {
		return err
	}

	// Gather data about the image and resize the last filesystem
	log.Infof("Shrinking filesystem...")
	err = shrink_fs(data)
	if err != nil {
		return err
	}

	// Print all of the relevant info
	s, err := yaml.Marshal(data)
	if err == nil {
		log.Infof("Image Data:\n%s\n", s)
	}

	// Shrink the parition and image file
	err = shrink_partition(data)
	if err != nil {
		return err
	}

	// Print parted info
	err = parted_print(data, "After parted")
	if err != nil {
		return err
	}

	// Check the partitons
	err = check_partition(data)
	if err != nil {
		return err
	}

	// Check the filesystems, if specified
	if check_diff {
		err = check_filesystem(data)
		if err != nil {
			return err
		}
	}

	// Looks like we're ok, remove the backup
	err = remove_backup(data)
	if err != nil {
		return err
	}

	return nil
}
