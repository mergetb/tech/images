package main

import (
	"encoding/json"
	"fmt"
)

type QemuArg [2]string

type Builder struct {
	Accelerator     string    `json:"accelerator"`
	BootCommand     []string  `json:"boot_command"`
	BootWait        string    `json:"boot_wait"`
	Cpus            int       `json:"cpus"`
	DiskInterface   string    `json:"disk_interface"`
	DiskSize        string    `json:"disk_size"`
	Firmware        string    `json:"firmware"`
	Format          string    `json:"format"`
	Headless        string    `json:"headless"`
	HttpDirectory   string    `json:"http_directory"`
	IsoUrl          string    `json:"iso_url"`
	IsoChecksum     string    `json:"iso_checksum"`
	Memory          int       `json:"memory"`
	OutputDirectory string    `json:"output_directory"`
	QemuArgs        []QemuArg `json:"qemuargs"`
	ShutdownCommand string    `json:"shutdown_command"`
	SshPassword     string    `json:"ssh_password"`
	SshTimeout      string    `json:"ssh_timeout"`
	SshUsername     string    `json:"ssh_username"`
	Type            string    `json:"type"`
	VmName          string    `json:"vm_name"`
}

func (p *Provisioner) MarshalJSON() ([]uint8, error) {
	switch p.Type {
	case "file":
		return json.Marshal(&struct {
			ProvisionerFile
			Type string `json:"type"`
		}{
			Type:            "file",
			ProvisionerFile: p.ProvisionerFile,
		})

	case "shell":
		return json.Marshal(&struct {
			ProvisionerShell
			Type string `json:"type"`
		}{
			Type:             "shell",
			ProvisionerShell: p.ProvisionerShell,
		})
	}

	return nil, fmt.Errorf("invalid type: %s", p.Type)
}

func (p *Provisioner) UnmarshalJSON(dat []uint8) error {
	switch p.Type {
	case "file":
		var pf struct {
			Type string
			ProvisionerFile
		}

		err := json.Unmarshal(dat, &pf)
		if err != nil {
			return err
		}

		p.ProvisionerFile = pf.ProvisionerFile
		return nil

	case "shell":
		var ps struct {
			Type string
			ProvisionerShell
		}

		err := json.Unmarshal(dat, &ps)
		if err != nil {
			return err
		}

		p.ProvisionerShell = ps.ProvisionerShell
		return nil
	}

	return fmt.Errorf("cannot unmarshal type %s", p.Type)
}

type ProvisionerFile struct {
	Destination string `json:"destination"`
	Direction   string `json:"direction,omitempty"`
	Source      string `json:"source"`
}

type ProvisionerShell struct {
	EnvironmentVars []string `json:"environment_vars"`
	Scripts         []string `json:"scripts"`
}

type Provisioner struct {
	Type string
	ProvisionerFile
	ProvisionerShell
}

type PackerConfig struct {
	Builders     []Builder         `json:"builders"`
	Provisioners []Provisioner     `json:"provisioners"`
	Variables    map[string]string `json:"variables,omitempty"`
}

type OSRoutines interface {
	GetPackerConfig(*Image) *PackerConfig
	GetOutputDirectory(*Image) string
	GetVmName(*Image) string
	GetIso(*Image) string
	GetJSON(*Image) string
}

type OSConfig struct {
	Efi bool
	Gui bool
}

type Image struct {
	Config *OSConfig
	OSRoutines
}
