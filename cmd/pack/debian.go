package main

import (
	"fmt"
)

const (
	BusterIso           = "debian-10.10.0-amd64-netinst.iso"
	BusterIsoChecksum   = "c433254a7c5b5b9e6a05f9e1379a0bd6ab3323f89b56537b684b6d1bd1f8b6ad"
	BullseyeIso         = "debian-11.6.0-amd64-netinst.iso"
	BullseyeIsoChecksum = "e482910626b30f9a7de9b0cc142c3d4a079fbfa96110083be1d0b473671ce08d"
)

type Buster struct{}
type Bullseye struct{}


func BullseyeBootCommand(i *Image) []string {
	if i.Config.Efi {
		return []string{
			"<wait10><wait10><wait10>",
			"<down><down>",
			"<enter>",
			"<down><down><down><down><down>",
			"<enter>",
			"<wait10><wait10><wait10><wait10>",
			"http://{{.HTTPIP}}:{{.HTTPPort}}/debian-bullseye/preseed.cfg ",
			"<enter>",
		}
	} else {
		return []string{
			"<esc><wait>",
			"install ",
			"auto ",
			"url=http://{{.HTTPIP}}:{{.HTTPPort}}/debian-bullseye/preseed-bios.cfg ",
			"debian-installer=en_US ",
			"locale=en_US ",
			"keymap=us ",
			"netcfg/get_hostname=lost ",
			"netcfg/get_domain=mergetb.io ",
			"<enter>",
		}
	}
}

func BullseyeDiskSize(i *Image) string {
	return "4096"
}

func debianScripts(i *Image, is_bullseye bool) []string {
	var scripts []string

	if i.Config.Efi {
		scripts = append(scripts, "../scripts/apt-efi.sh")
	} else {
		scripts = append(scripts, "../scripts/apt-bios.sh")
	}

	scripts = append(scripts, []string{
		"../scripts/journald.sh",
		"../scripts/sshd.sh",
		"../scripts/cleanup.sh",
		"scripts/network.sh",
		"../scripts/foundry.sh",
		"../scripts/collect.sh",
		"../scripts/jupyterhub.sh",
	}...,
	)

	if i.Config.Efi {
		scripts = append(scripts, "../scripts/part-uuid-efi.sh")
	} else {
		scripts = append(scripts, "../scripts/part-uuid-bios.sh")
	}

	scripts = append(scripts, "../scripts/resolved.sh")

	if i.Config.Efi {
		scripts = append(scripts, []string{"../scripts/grub-efi.sh", "../scripts/efivars.sh"}...)
	} else {
		scripts = append(scripts, "../scripts/grub-bios.sh")
	}

	scripts = append(scripts, "../scripts/minimize.sh")

	return scripts
}

func debianProvisioners(i *Image, dir string, is_bullseye bool) []Provisioner {
	if i.Config.Efi {
		return []Provisioner{
			Provisioner{
				Type: "file",
				ProvisionerFile: ProvisionerFile{
					Source:      "../files/efivars-remount.service",
					Destination: "/tmp/efivars-remount.service",
				},
			},
			Provisioner{
				Type: "file",
				ProvisionerFile: ProvisionerFile{
					Source:      "../files/efivars-remount.sh",
					Destination: "/tmp/efivars-remount.sh",
				},
			},
			Provisioner{
				Type: "shell",
				ProvisionerShell: ProvisionerShell{
					EnvironmentVars: []string{
						"CMDLINE=8250.nr_uarts=1",
						"STTY=ttyS0",
						"OS=debian",
					},
					Scripts: debianScripts(i, is_bullseye),
				},
			},
			Provisioner{
				Type: "file",
				ProvisionerFile: ProvisionerFile{
					Source:      "/tmp/collect/",
					Destination: dir,
					Direction:   "download",
				},
			},
		}
	} else {
		return []Provisioner{
			Provisioner{
				Type: "shell",
				ProvisionerShell: ProvisionerShell{
					EnvironmentVars: []string{
						"STTY=ttyS1",
						"OS=debian",
					},
					Scripts: debianScripts(i, is_bullseye),
				},
			},
			Provisioner{
				Type: "file",
				ProvisionerFile: ProvisionerFile{
					Source:      "/tmp/collect/",
					Destination: dir,
					Direction:   "download",
				},
			},
		}
	}
}

func (u *Bullseye) GetPackerConfig(i *Image) *PackerConfig {
	cfg := &PackerConfig{
		Builders: []Builder{
			Builder{
				Accelerator:     "kvm",
				BootCommand:     BullseyeBootCommand(i),
				BootWait:        "5s",
				Cpus:            2,
				DiskInterface:   "ide",
				DiskSize:        BullseyeDiskSize(i),
				Format:          "raw",
				Headless:        "true",
				HttpDirectory:   "http",
				IsoUrl:          fmt.Sprintf("iso/%s", BullseyeIso),
				IsoChecksum:     fmt.Sprintf("sha256:%s", BullseyeIsoChecksum),
				Memory:          2048,
				OutputDirectory: u.GetOutputDirectory(i),
				ShutdownCommand: "sudo systemctl poweroff",
				SshPassword:     "test",
				SshTimeout:      "60m",
				SshUsername:     "test",
				Type:            "qemu",
				VmName:          u.GetVmName(i),
			},
		},
		Provisioners: debianProvisioners(i, u.GetOutputDirectory(i), true),
	}

	if i.Config.Efi {
		cfg.Builders[0].Firmware = "/usr/share/edk2/ovmf/OVMF_CODE.fd"
	}

	return cfg
}

func (u *Bullseye) GetOutputDirectory(i *Image) string {
	if i.Config.Efi {
		return "build/bullseye/efi"
	} else {
		return "build/bullseye/bios"
	}
}

func (u *Bullseye) GetVmName(i *Image) string {
	return "debian-bullseye"
}

func (u *Bullseye) GetIso(i *Image) string {
	return BullseyeIso
}

func (u *Bullseye) GetJSON(i *Image) string {
	var s string

	if i.Config.Efi {
		s = "bullseye-efi"
	} else {
		s = "bullseye-bios"
	}

	return "debian/" + s + ".json"
}

func BusterBootCommand(i *Image) []string {
	return BullseyeBootCommand(i)
}

func BusterDiskSize(i *Image) string {
	return "4096"
}

func (u *Buster) GetPackerConfig(i *Image) *PackerConfig {
	cfg := &PackerConfig{
		Builders: []Builder{
			Builder{
				Accelerator:     "kvm",
				BootCommand:     BusterBootCommand(i),
				BootWait:        "5s",
				Cpus:            2,
				DiskInterface:   "ide",
				DiskSize:        BusterDiskSize(i),
				Format:          "raw",
				Headless:        "true",
				HttpDirectory:   "http",
				IsoUrl:          fmt.Sprintf("iso/%s", BusterIso),
				IsoChecksum:     fmt.Sprintf("sha256:%s", BusterIsoChecksum),
				Memory:          2048,
				OutputDirectory: u.GetOutputDirectory(i),
				ShutdownCommand: "sudo systemctl poweroff",
				SshPassword:     "test",
				SshTimeout:      "60m",
				SshUsername:     "test",
				Type:            "qemu",
				VmName:          u.GetVmName(i),
			},
		},
		Provisioners: debianProvisioners(i, u.GetOutputDirectory(i), false),
	}

	if i.Config.Efi {
		cfg.Builders[0].Firmware = "/usr/share/edk2/ovmf/OVMF_CODE.fd"
	}

	return cfg
}

func (u *Buster) GetOutputDirectory(i *Image) string {
	if i.Config.Efi {
		return "build/buster/efi"
	} else {
		return "build/buster/bios"
	}
}

func (u *Buster) GetVmName(i *Image) string {
	return "debian-buster"
}

func (u *Buster) GetIso(i *Image) string {
	return BusterIso
}

func (u *Buster) GetJSON(i *Image) string {
	var s string

	if i.Config.Efi {
		s = "buster-efi"
	} else {
		s = "buster-bios"
	}

	return "debian/" + s + ".json"
}
