package main

import (
	"fmt"
)

// Tinycore12

const (
	Tinycore12Iso         = "tc12/CorePure64-12.0.iso"
	Tinycore12IsoChecksum = "ff7d97b755fcef1a88c582a48e3153ee800d4dfbe87a5d945338ecabd70dd217"
)

type Tinycore12 struct{}

func Tinycore12BootCommand(i *Image) []string {
	if i.Config.Efi {
		return []string{
			"<enter><wait10><enter>",
			"wget http://{{.HTTPIP}}:{{.HTTPPort}}/1-init-efi.sh; ",
			"wget http://{{.HTTPIP}}:{{.HTTPPort}}/2-init-common.sh; ",
			"chmod +x *.sh; ",
			"./1-init-efi.sh && ./2-init-common.sh",
			"<enter>",
		}
	} else {
		return []string{
			"<enter><wait10><enter>",
			"wget http://{{.HTTPIP}}:{{.HTTPPort}}/1-init-bios.sh; ",
			"wget http://{{.HTTPIP}}:{{.HTTPPort}}/2-init-common.sh; ",
			"chmod +x *.sh; ",
			"./1-init-bios.sh && ./2-init-common.sh ",
			"<enter>",
		}
	}
}

func Tinycore12DiskSize(i *Image) string {
	return "100"
}

func Tinycore12QemuArgs(i *Image) []QemuArg {
	var args []QemuArg

	// as the iso only works on bios boot,
	// add the kernel and initramfs to the qemu call
	// to make booting consistent
	return append(args,
		QemuArg{
			"-kernel",
			"iso/tc12/vmlinuz64",
		},
		QemuArg{
			"-initrd",
			"iso/tc12/corepure64.gz",
		},
	)
}

func Tinycore12Scripts(i *Image) []string {
	var scripts []string

	if i.Config.Efi {
		scripts = append(scripts,
			"scripts/3-grub-efi.sh",
			"scripts/4-foundry-efi.sh",
		)
	} else {
		scripts = append(scripts,
			"scripts/3-grub-bios.sh",
			"scripts/4-foundry-bios.sh",
		)
	}

	scripts = append(scripts,
		"scripts/5-cleanup.sh",
	)

	return scripts
}

func Tinycore12Provisioners(i *Image, dir string) []Provisioner {
	if i.Config.Efi {
		return []Provisioner{
			Provisioner{
				Type: "shell",
				ProvisionerShell: ProvisionerShell{
					EnvironmentVars: []string{
						"CMDLINE=8250.nr_uarts=1",
						"STTY=ttyS0",
					},
					Scripts: Tinycore12Scripts(i),
				},
			},
			Provisioner{
				Type: "file",
				ProvisionerFile: ProvisionerFile{
					Source:      "/tmp/collect/",
					Destination: dir,
					Direction:   "download",
				},
			},
		}
	} else {
		return []Provisioner{
			Provisioner{
				Type: "shell",
				ProvisionerShell: ProvisionerShell{
					EnvironmentVars: []string{
						"STTY=ttyS1",
					},
					Scripts: Tinycore12Scripts(i),
				},
			},
			Provisioner{
				Type: "file",
				ProvisionerFile: ProvisionerFile{
					Source:      "/tmp/collect/",
					Destination: dir,
					Direction:   "download",
				},
			},
		}
	}
}

func (u *Tinycore12) GetPackerConfig(i *Image) *PackerConfig {
	return &PackerConfig{
		Builders: []Builder{
			Builder{
				Accelerator:     "kvm",
				BootCommand:     Tinycore12BootCommand(i),
				BootWait:        "5s",
				Cpus:            2,
				DiskInterface:   "ide",
				DiskSize:        Tinycore12DiskSize(i),
				Format:          "raw",
				Headless:        "true",
				HttpDirectory:   "scripts",
				IsoUrl:          fmt.Sprintf("iso/%s", Tinycore12Iso),
				IsoChecksum:     fmt.Sprintf("sha256:%s", Tinycore12IsoChecksum),
				Memory:          2048,
				OutputDirectory: u.GetOutputDirectory(i),
				QemuArgs:        Tinycore12QemuArgs(i),
				ShutdownCommand: "sudo poweroff",
				SshPassword:     "vmuser#123",
				SshTimeout:      "60m",
				SshUsername:     "tc",
				Type:            "qemu",
				VmName:          u.GetVmName(i),
			},
		},
		Provisioners: Tinycore12Provisioners(i, u.GetOutputDirectory(i)),
	}
}

func (u *Tinycore12) GetOutputDirectory(i *Image) string {
	if i.Config.Efi {
		return "build/tc12/efi"
	} else {
		return "build/tc12/bios"
	}
}

func (u *Tinycore12) GetVmName(i *Image) string {
	return "tinycore-tc12"
}

func (u *Tinycore12) GetIso(i *Image) string {
	return Tinycore12Iso
}

func (u *Tinycore12) GetJSON(i *Image) string {
	var s string

	if i.Config.Efi {
		s = "tc12-efi"
	} else {
		s = "tc12-bios"
	}

	return "tinycore/" + s + ".json"
}

// Tinycore13

const (
	Tinycore13Iso         = "tc13/CorePure64-13.1.iso"
	Tinycore13IsoChecksum = "baa89ec76f8f120b3b49c143a28cc81a6bb2ea7b32bdd2691ab30dce72e90de0"
)

type Tinycore13 struct{}

func Tinycore13BootCommand(i *Image) []string {
	if i.Config.Efi {
		return []string{
			"<enter><wait10><enter>",
			"wget http://{{.HTTPIP}}:{{.HTTPPort}}/1-init-efi.sh; ",
			"wget http://{{.HTTPIP}}:{{.HTTPPort}}/2-init-common.sh; ",
			"chmod +x *.sh; ",
			"./1-init-efi.sh && ./2-init-common.sh",
			"<enter>",
		}
	} else {
		return []string{
			"<enter><wait10><enter>",
			"wget http://{{.HTTPIP}}:{{.HTTPPort}}/1-init-bios.sh; ",
			"wget http://{{.HTTPIP}}:{{.HTTPPort}}/2-init-common.sh; ",
			"chmod +x *.sh; ",
			"./1-init-bios.sh && ./2-init-common.sh ",
			"<enter>",
		}
	}
}

func Tinycore13DiskSize(i *Image) string {
	return "100"
}

func Tinycore13QemuArgs(i *Image) []QemuArg {
	var args []QemuArg

	return append(args,
		QemuArg{
			"-kernel",
			"iso/tc13/vmlinuz64",
		},
		QemuArg{
			"-initrd",
			"iso/tc13/corepure64.gz",
		},
	)
}

func Tinycore13Scripts(i *Image) []string {
	var scripts []string

	if i.Config.Efi {
		scripts = append(scripts,
			"scripts/3-grub-efi.sh",
			"scripts/4-foundry-efi.sh",
		)
	} else {
		scripts = append(scripts,
			"scripts/3-grub-bios.sh",
			"scripts/4-foundry-bios.sh",
		)
	}

	scripts = append(scripts,
		"scripts/5-cleanup.sh",
	)

	return scripts
}

func Tinycore13Provisioners(i *Image, dir string) []Provisioner {
	if i.Config.Efi {
		return []Provisioner{
			Provisioner{
				Type: "shell",
				ProvisionerShell: ProvisionerShell{
					EnvironmentVars: []string{
						"CMDLINE=8250.nr_uarts=1",
						"STTY=ttyS0",
					},
					Scripts: Tinycore13Scripts(i),
				},
			},
			Provisioner{
				Type: "file",
				ProvisionerFile: ProvisionerFile{
					Source:      "/tmp/collect/",
					Destination: dir,
					Direction:   "download",
				},
			},
		}
	} else {
		return []Provisioner{
			Provisioner{
				Type: "shell",
				ProvisionerShell: ProvisionerShell{
					EnvironmentVars: []string{
						"STTY=ttyS1",
					},
					Scripts: Tinycore13Scripts(i),
				},
			},
			Provisioner{
				Type: "file",
				ProvisionerFile: ProvisionerFile{
					Source:      "/tmp/collect/",
					Destination: dir,
					Direction:   "download",
				},
			},
		}
	}
}

func (u *Tinycore13) GetPackerConfig(i *Image) *PackerConfig {
	return &PackerConfig{
		Builders: []Builder{
			Builder{
				Accelerator:     "kvm",
				BootCommand:     Tinycore13BootCommand(i),
				BootWait:        "5s",
				Cpus:            2,
				DiskInterface:   "ide",
				DiskSize:        Tinycore13DiskSize(i),
				Format:          "raw",
				Headless:        "true",
				HttpDirectory:   "scripts",
				IsoUrl:          fmt.Sprintf("iso/%s", Tinycore13Iso),
				IsoChecksum:     fmt.Sprintf("sha256:%s", Tinycore13IsoChecksum),
				Memory:          2048,
				OutputDirectory: u.GetOutputDirectory(i),
				QemuArgs:        Tinycore13QemuArgs(i),
				ShutdownCommand: "sudo poweroff",
				SshPassword:     "vmuser#123",
				SshTimeout:      "60m",
				SshUsername:     "tc",
				Type:            "qemu",
				VmName:          u.GetVmName(i),
			},
		},
		Provisioners: Tinycore13Provisioners(i, u.GetOutputDirectory(i)),
	}
}

func (u *Tinycore13) GetOutputDirectory(i *Image) string {
	if i.Config.Efi {
		return "build/tc13/efi"
	} else {
		return "build/tc13/bios"
	}
}

func (u *Tinycore13) GetVmName(i *Image) string {
	return "tinycore-tc13"
}

func (u *Tinycore13) GetIso(i *Image) string {
	return Tinycore13Iso
}

func (u *Tinycore13) GetJSON(i *Image) string {
	var s string

	if i.Config.Efi {
		s = "tc13-efi"
	} else {
		s = "tc13-bios"
	}

	return "tinycore/" + s + ".json"
}
