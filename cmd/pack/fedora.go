package main

import (
	"fmt"
)

const (
	FedoraIso         = "Fedora-Server-netinst-x86_64-39-1.5.iso"
	FedoraIsoChecksum = "61576ae7170e35210f03aae3102048f0a9e0df7868ac726908669b4ef9cc22e9"
)

type Fedora struct{}

func FedoraBootCommand(i *Image) []string {
	if i.Config.Efi {
		return []string{
			"<up>e",
			"<wait1>",
			"<down><down>",
			"<leftCtrlOn>k<leftCtrlOff>",
			"        linuxefi /images/pxeboot/vmlinuz",
			" inst.ks=http://{{.HTTPIP}}:{{.HTTPPort}}/hypervisor/anaconda-ks.cfg",
			"<leftCtrlOn>x<leftCtrlOff>",
		}
	} else {
		return []string{
			"<up>e",
			"<wait1>",
			"<down><down>",
			"<leftCtrlOn>k<leftCtrlOff>",
			"        linux /images/pxeboot/vmlinuz",
			" inst.ks=http://{{.HTTPIP}}:{{.HTTPPort}}/hypervisor/anaconda-ks-bios.cfg",
			"<leftCtrlOn>x<leftCtrlOff>",
		}
	}
}

func FedoraDiskSize(i *Image) string {
	return "8000"
}

func FedoraScripts(i *Image) []string {
	var scripts []string

	scripts = append(scripts, []string{
		"scripts/selinux.sh",
		"scripts/nvme.sh",
		"scripts/dnf.sh",
		"../scripts/journald.sh",
		"../scripts/sshd.sh",
		"../scripts/cleanup.sh",
		"scripts/network.sh",
		"scripts/firewalld.sh",
		"scripts/mars.sh",
		"scripts/sysctl.sh",
		"../scripts/foundry.sh",
		"../scripts/collect.sh",
		"../scripts/resolved.sh",
	}...)

	if i.Config.Efi {
		scripts = append(scripts, []string{
			"../scripts/part-uuid-efi.sh",
			"../scripts/grub-efi.sh",
			"../scripts/efivars.sh",
		}...)
	} else {
		scripts = append(scripts, []string{
			"../scripts/part-uuid-bios.sh",
			"../scripts/grub-bios.sh",
		}...)
	}

	scripts = append(scripts, "../scripts/minimize.sh")

	return scripts
}

func FedoraProvisioners(i *Image, dir string) []Provisioner {
	if i.Config.Efi {
		return []Provisioner{
			Provisioner{
				Type: "file",
				ProvisionerFile: ProvisionerFile{
					Source:      "../files/efivars-remount.service",
					Destination: "/tmp/efivars-remount.service",
				},
			},
			Provisioner{
				Type: "file",
				ProvisionerFile: ProvisionerFile{
					Source:      "../files/efivars-remount.sh",
					Destination: "/tmp/efivars-remount.sh",
				},
			},
			Provisioner{
				Type: "shell",
				ProvisionerShell: ProvisionerShell{
					EnvironmentVars: []string{
						"CMDLINE=8250.nr_uarts=1",
						"STTY=ttyS0",
						"OS=fedora",
					},
					Scripts: FedoraScripts(i),
				},
			},
			Provisioner{
				Type: "file",
				ProvisionerFile: ProvisionerFile{
					Source:      "/tmp/collect/",
					Destination: dir,
					Direction:   "download",
				},
			},
		}
	} else {
		return []Provisioner{
			Provisioner{
				Type: "shell",
				ProvisionerShell: ProvisionerShell{
					EnvironmentVars: []string{
						"STTY=ttyS1",
						"OS=fedora",
					},
					Scripts: FedoraScripts(i),
				},
			},
			Provisioner{
				Type: "file",
				ProvisionerFile: ProvisionerFile{
					Source:      "/tmp/collect/",
					Destination: dir,
					Direction:   "download",
				},
			},
		}
	}
}

func (u *Fedora) GetPackerConfig(i *Image) *PackerConfig {
	cfg := &PackerConfig{
		Builders: []Builder{
			Builder{
				Accelerator:     "kvm",
				BootCommand:     FedoraBootCommand(i),
				BootWait:        "10s",
				Cpus:            2,
				DiskInterface:   "ide",
				DiskSize:        FedoraDiskSize(i),
				Format:          "raw",
				Headless:        "true",
				HttpDirectory:   "http",
				IsoUrl:          fmt.Sprintf("iso/%s", FedoraIso),
				IsoChecksum:     fmt.Sprintf("sha256:%s", FedoraIsoChecksum),
				Memory:          2048,
				OutputDirectory: u.GetOutputDirectory(i),
				ShutdownCommand: "sudo systemctl poweroff",
				SshPassword:     "test",
				SshTimeout:      "60m",
				SshUsername:     "test",
				Type:            "qemu",
				VmName:          u.GetVmName(i),
			},
		},
		Provisioners: FedoraProvisioners(i, u.GetOutputDirectory(i)),
	}

	if i.Config.Efi {
		cfg.Builders[0].Firmware = "/usr/share/edk2/ovmf/OVMF_CODE.fd"
	}

	return cfg
}

func (u *Fedora) GetOutputDirectory(i *Image) string {
	if i.Config.Efi {
		return "build/hypervisor/efi"
	} else {
		return "build/hypervisor/bios"
	}
}

func (u *Fedora) GetVmName(i *Image) string {
	return "fedora-hypervisor"
}

func (u *Fedora) GetIso(i *Image) string {
	return FedoraIso
}

func (u *Fedora) GetJSON(i *Image) string {
	if i.Config.Efi {
		return "fedora/hypervisor-efi.json"
	} else {
		return "fedora/hypervisor-bios.json"
	}
}
