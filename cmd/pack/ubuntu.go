package main

import (
	"fmt"
	"strings"
)

const (
	Ubuntu1804Iso         = "ubuntu-18.04.4-server-amd64.iso"
	Ubuntu1804IsoChecksum = "e2ecdace33c939527cbc9e8d23576381c493b071107207d2040af72595f8990b"
	Ubuntu2004Iso         = "ubuntu-20.04.1-legacy-server-amd64.iso"
	Ubuntu2004IsoChecksum = "f11bda2f2caed8f420802b59f382c25160b114ccc665dbac9c5046e7fceaced2"
)

type Ubuntu1804 struct{}
type Ubuntu2004 struct{}

func Ubuntu1804BootCommand(i *Image) []string {
	if i.Config.Efi {
		return []string{
			"<wait5>",
			"e",
			"<wait5>",
			"<down><down><down>",
			"<leftCtrlOn>k<leftCtrlOff>",
			"        linux         /install/vmlinuz ",
			"--- ",
			"auto=true ",
			"url=http://{{.HTTPIP}}:{{.HTTPPort}}/ubuntu-1804/preseed.cfg ",
			"hostname=lost ",
			"domain=mergetb.io ",
			"priority=critical",
			"<F10>",
		}
	} else {
		return []string{
			"<esc><esc><enter><wait>",
			"/install/vmlinuz noapic ",
			"initrd=/install/initrd.gz ",
			"debian-installer=en_US auto locale=en_US kbd-chooser/method=us ",
			"auto=true ",
			"url=http://{{.HTTPIP}}:{{.HTTPPort}}/ubuntu-1804/preseed-bios.cfg ",
			"hostname=lost ",
			"grub-installer/bootdev=/dev/sda<wait> ",
			"fb=false debconf/frontend=noninteractive ",
			"keyboard-configuration/modelcode=SKIP keyboard-configuration/layout=USA ",
			"keyboard-configuration/variant=USA console-setup/ask_detect=false ",
			"passwd/user-fullname=test ",
			"passwd/user-password=test ",
			"passwd/user-password-again=test ",
			"passwd/username=test ",
			"-- <enter>",
		}
	}
}

func Ubuntu1804DiskSize(i *Image) string {
	return "4096"
}

func Ubuntu1804Scripts(i *Image) []string {
	var scripts []string

	if i.Config.Efi {
		scripts = append(scripts, "../scripts/apt-efi.sh")
	} else {
		scripts = append(scripts, "../scripts/apt-bios.sh")
	}

	scripts = append(scripts, "scripts/bionic-apt.sh")

	scripts = append(scripts, []string{
		"../scripts/journald.sh",
		"../scripts/sshd.sh",
		"../scripts/cleanup.sh",
		"scripts/network.sh",
		"../scripts/foundry.sh",
		"../scripts/collect.sh",
		"../scripts/jupyterhub.sh",
	}...,
	)

	if i.Config.Efi {
		scripts = append(scripts, "../scripts/part-uuid-efi.sh")
	} else {
		scripts = append(scripts, "../scripts/part-uuid-bios.sh")
	}

	scripts = append(scripts, "../scripts/resolved.sh")

	if i.Config.Efi {
		scripts = append(scripts, []string{"../scripts/grub-efi.sh", "../scripts/efivars.sh"}...)
	} else {
		scripts = append(scripts, "../scripts/grub-bios.sh")
	}

	scripts = append(scripts, "../scripts/minimize.sh")

	return scripts
}

func Ubuntu1804Provisioners(i *Image, dir string) []Provisioner {
	if i.Config.Efi {
		return []Provisioner{
			Provisioner{
				Type: "file",
				ProvisionerFile: ProvisionerFile{
					Source:      "../files/efivars-remount.service",
					Destination: "/tmp/efivars-remount.service",
				},
			},
			Provisioner{
				Type: "file",
				ProvisionerFile: ProvisionerFile{
					Source:      "../files/efivars-remount.sh",
					Destination: "/tmp/efivars-remount.sh",
				},
			},
			Provisioner{
				Type: "shell",
				ProvisionerShell: ProvisionerShell{
					EnvironmentVars: []string{
						"CMDLINE=8250.nr_uarts=1",
						"STTY=ttyS0",
						"OS=ubuntu",
					},
					Scripts: Ubuntu1804Scripts(i),
				},
			},
			Provisioner{
				Type: "file",
				ProvisionerFile: ProvisionerFile{
					Source:      "/tmp/collect/",
					Destination: dir,
					Direction:   "download",
				},
			},
		}
	} else {
		return []Provisioner{
			Provisioner{
				Type: "shell",
				ProvisionerShell: ProvisionerShell{
					EnvironmentVars: []string{
						"STTY=ttyS1",
						"OS=ubuntu",
					},
					Scripts: Ubuntu1804Scripts(i),
				},
			},
			Provisioner{
				Type: "file",
				ProvisionerFile: ProvisionerFile{
					Source:      "/tmp/collect/",
					Destination: dir,
					Direction:   "download",
				},
			},
		}
	}
}

func (u *Ubuntu1804) GetPackerConfig(i *Image) *PackerConfig {
	cfg := &PackerConfig{
		Builders: []Builder{
			Builder{
				Accelerator:     "kvm",
				BootCommand:     Ubuntu1804BootCommand(i),
				BootWait:        "5s",
				Cpus:            2,
				DiskInterface:   "ide",
				DiskSize:        Ubuntu1804DiskSize(i),
				Format:          "raw",
				Headless:        "true",
				HttpDirectory:   "http",
				IsoUrl:          fmt.Sprintf("iso/%s", Ubuntu1804Iso),
				IsoChecksum:     fmt.Sprintf("sha256:%s", Ubuntu1804IsoChecksum),
				Memory:          2048,
				OutputDirectory: u.GetOutputDirectory(i),
				ShutdownCommand: "sudo systemctl poweroff",
				SshPassword:     "test",
				SshTimeout:      "60m",
				SshUsername:     "test",
				Type:            "qemu",
				VmName:          u.GetVmName(i),
			},
		},
		Provisioners: Ubuntu1804Provisioners(i, u.GetOutputDirectory(i)),
	}

	if i.Config.Efi {
		cfg.Builders[0].Firmware = "/usr/share/edk2/ovmf/OVMF_CODE.fd"
	}

	return cfg
}

func (u *Ubuntu1804) GetOutputDirectory(i *Image) string {
	if i.Config.Efi {
		return "build/1804/efi"
	} else {
		return "build/1804/bios"
	}
}

func (u *Ubuntu1804) GetVmName(i *Image) string {
	return "ubuntu-1804"
}

func (u *Ubuntu1804) GetIso(i *Image) string {
	return Ubuntu1804Iso
}

func (u *Ubuntu1804) GetJSON(i *Image) string {
	var s string

	if i.Config.Efi {
		s = "1804-efi"
	} else {
		s = "1804-bios"
	}

	return "ubuntu/" + s + ".json"
}

func Ubuntu2004BootCommand(i *Image) []string {
	return Ubuntu1804BootCommand(i)
}

func Ubuntu2004DiskSize(i *Image) string {
	return "4096"
}

func Ubuntu2004Provisioners(i *Image, dir string) []Provisioner {
	provisioners := Ubuntu1804Provisioners(i, dir)

	// remove the call to bionic-apt.sh
	for idx, p := range provisioners {
		if p.Type == "shell" {
			scripts := []string{}
			for _, s := range p.Scripts {
				if !strings.Contains(s, "bionic-apt") {
					scripts = append(scripts, s)
				}
			}
			provisioners[idx].Scripts = scripts
		}
	}

	return provisioners
}

func (u *Ubuntu2004) GetPackerConfig(i *Image) *PackerConfig {
	cfg := &PackerConfig{
		Builders: []Builder{
			Builder{
				Accelerator:     "kvm",
				BootCommand:     Ubuntu2004BootCommand(i),
				BootWait:        "5s",
				Cpus:            2,
				DiskInterface:   "ide",
				DiskSize:        Ubuntu2004DiskSize(i),
				Format:          "raw",
				Headless:        "true",
				HttpDirectory:   "http",
				IsoUrl:          fmt.Sprintf("iso/%s", Ubuntu2004Iso),
				IsoChecksum:     fmt.Sprintf("sha256:%s", Ubuntu2004IsoChecksum),
				Memory:          2048,
				OutputDirectory: u.GetOutputDirectory(i),
				ShutdownCommand: "sudo systemctl poweroff",
				SshPassword:     "test",
				SshTimeout:      "60m",
				SshUsername:     "test",
				Type:            "qemu",
				VmName:          u.GetVmName(i),
			},
		},
		Provisioners: Ubuntu2004Provisioners(i, u.GetOutputDirectory(i)),
	}

	if i.Config.Efi {
		cfg.Builders[0].Firmware = "/usr/share/edk2/ovmf/OVMF_CODE.fd"
	}

	return cfg
}

func (u *Ubuntu2004) GetOutputDirectory(i *Image) string {
	if i.Config.Efi {
		return "build/2004/efi"
	} else {
		return "build/2004/bios"
	}
}

func (u *Ubuntu2004) GetVmName(i *Image) string {
	return "ubuntu-2004"
}

func (u *Ubuntu2004) GetIso(i *Image) string {
	return Ubuntu2004Iso
}

func (u *Ubuntu2004) GetJSON(i *Image) string {
	var s string

	if i.Config.Efi {
		s = "2004-efi"
	} else {
		s = "2004-bios"
	}

	return "ubuntu/" + s + ".json"
}
