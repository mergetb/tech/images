package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

func buildConfigs() []*OSConfig {
	var configs []*OSConfig

	configs = append(configs, &OSConfig{Efi: true})
	configs = append(configs, &OSConfig{Efi: false})

	return configs
}

func getImages() []*Image {
	var images []*Image

	configs := buildConfigs()

	for _, c := range configs {
		images = append(images, &Image{
			Config:     c,
			OSRoutines: &Ubuntu1804{},
		})

		images = append(images, &Image{
			Config:     c,
			OSRoutines: &Ubuntu2004{},
		})

		images = append(images, &Image{
			Config:     c,
			OSRoutines: &Bullseye{},
		})

		images = append(images, &Image{
			Config:     c,
			OSRoutines: &Buster{},
		})

		images = append(images, &Image{
			Config:     c,
			OSRoutines: &Fedora{},
		})

		images = append(images, &Image{
			Config:     c,
			OSRoutines: &Tinycore12{},
		})

		images = append(images, &Image{
			Config:     c,
			OSRoutines: &Tinycore13{},
		})
	}

	return images
}

func main() {
	images := getImages()

	for _, img := range images {
		pck := img.GetPackerConfig(img)

		if pck == nil {
			continue
		}

		b := bytes.NewBuffer([]byte{})
		enc := json.NewEncoder(b)
		enc.SetEscapeHTML(false)
		enc.Encode(pck)

		/*
		   dat, err := json.MarshalIndent(pck, "", "    ")
		   if err != nil {
		       fmt.Errorf("failed to marshal cfg: %v", err)
		       os.Exit(1)
		   }
		*/

		var out bytes.Buffer
		err := json.Indent(&out, b.Bytes(), "", "    ")
		if err != nil {
			fmt.Errorf("failed to indent json: %v", err)
			os.Exit(1)
		}

		err = ioutil.WriteFile(img.GetJSON(img), out.Bytes(), 0644)
		if err != nil {
			fmt.Errorf("faild to write to file: %v", err)
			os.Exit(1)
		}
	}
}
