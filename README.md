# MergeTB Image Creation

## Overview

MergeTB OS images are created with the [HashiCorp Packer](https://www.packer.io/) tool. For each OS
disitribution we support, a separate packer configuration file is needed. Furthermore, we currently
generate separate configurations for each boot variant: one for EFI, BIOS, VM machines.

Currently, we support the following distributions:
- Ubuntu 18.04 and 20.04
- Debian "Buster" (10.10.0) and "Bullseye" (11.3.0)
- Alpine Linux (3.14.0)
- Tinycore Linux, 12.0 and 13.1
- Fedora 34 which runs the Merge hypervisor software

## Running without root
You do not need root access if the user is in the group: `libvirt`.

## Configuration

Each (distribution,boot variant) pair has a packer configuration file that defines:
- which ISO image to install from
- Installation-time configuration (disk partitions, account configuration, etc.)
- Post-installation configuration
    - Package updates
    - Installation of Merge services (e.g. `foundryc`)
    - Network configuration

Packer automates these operations, obviating the need to have a human in the loop. Packer installs
an image by booting it into a Qemu/KVM virtual machine and then executing the commands you write for
it.

## Example: Debian Bullseye EFI

Let's walkthrough the interesting bits of the [Debian Bullseye EFI configuration](debian/bullseye-efi.json)

### `boot_command`

```
"boot_command": [
    "<wait10><wait10><wait10>",
    "<down><down>",
    "<enter>",
    "<down><down><down><down><down>",
    "<enter>",
    "<wait10><wait10><wait10><wait10>",
    "http://{{.HTTPIP}}:{{.HTTPPort}}/debian-bullseye/preseed.cfg ",
    "<enter>"
]
```

The `boot_command` defines key presses that packer will enter once it boots a VM. This is pretty
self explanatory. `<wait10>` pauses for 10 seconds, `<down>` enters the down key, etc.

This line is notable:
```
"http://{{.HTTPIP}}:{{.HTTPPort}}/debian-bullseye/preseed.cfg "
```

This is telling the OS to install from a `preseed` configuration, which is a selection of
configuration options that the Debian installer would normally expect an interactive user to
provide. On your host, Packer listens on `HTTPIP`:`HTTPPort`, and so serves this preseed file over
http.

### `iso_url` and `iso_checksum`

```
"iso_url": "iso/debian-11.1.0-amd64-netinst.iso",
"iso_checksum": "sha256:8488abc1361590ee7a3c9b00ec059b29dfb1da40f8ba4adf293c7a30fa943eb2",
```

These tell Packer what image to boot from and what its sha256 checksum should be

### `output_directory`

`output_directory` is important, as it determines where the resulting OS image will be placed. Our
CI process generates artifacts from this directory, so it should be unique for each image you build.


### Provisioners

```
"provisioners": [
    {
        "destination": "/tmp/efivars-remount.service",
        "source": "files/efivars-remount.service",
        "type": "file"
    },
    {
        "destination": "/tmp/efivars-remount.sh",
        "source": "files/efivars-remount.sh",
        "type": "file"
    },
    {
        "environment_vars": [
            "CMDLINE=8250.nr_uarts=1",
            "STTY=ttyS0",
            "OS=debian"
        ],
        "scripts": [
            "../scripts/apt.sh",
            "../scripts/journald.sh",
            "../scripts/sshd.sh",
            "../scripts/cleanup.sh",
            "scripts/network.sh",
            "../scripts/collect.sh",
            "../scripts/part-uuid.sh",
            "../scripts/resolved.sh",
            "scripts/grub.sh",
            "../scripts/efivars.sh",
            "../scripts/minimize.sh"
        ],
        "type": "shell"
    },
    {
        "destination": "build/bullseye/efi",
        "direction": "download",
        "source": "/tmp/collect/",
        "type": "file"
    }
]
```

Provisioners are post installation scripts that run once the base OS image is installed. Generally,
we use provisioners to make the images MergeTB capable. A few notable steps include:

- `files/efivars-remount.service`: install a systemd service to mount EFI variables on each boot
- `../scripts/apt.sh` : install a few apt packages
- `../scripts/foundry.sh` install Merge's `foundryc` client package
- `scripts/network.sh` : Enable `systemd-networkd` and `systemd-resolved`
- `scripts/grub.sh` : Update bootloader settings for the Merge boot process


## Golang-based configuration generation

Because we support a number of distributions and boot variants, there is a relatively large number
of Packer configurations, with significant overlap between them. For this reason, we have a Go
binary program to generate the Packer configurations from source code. This greatly reduces
configuration errors and makes it easy to update a large number of Packer configurations if and when
a small piece of the installation process changes (e.g., if the process for installing `foundry`
changes) or new distributions are supported.


[cmd/pack](./cmd/pack) is where the code lives. There are separate files for each distribution. The code
should be pretty self explanatory.

To build:
```
make bin/pack
```

To run:
```
./bin/pack
```

This will update the Packer JSON files that live in `debian/`, `ubuntu/`, etc.

## bin/shrink: Golang-based Image Shrinking

It's possible for an image's raw logical size to be much greater than the image's physical size.
This happens when the image's main filesystem is mostly unused, for example, 3 GB used, but 8 GB allocated in the image's partitions.

Although this largely doesn't affect compression time or size (since it should be zero'd out),
this can have a large effect when we decompress the image for use in Sled, as we must write out the entire image, even if most of it is zero.

The simplest way to deal with this is to allocate a smaller disk, but some distributions, most notably Fedora,
have a minimum size requirement of the disk during installation, even if we only end up using a fraction of the space allocated.

To counteract this, there is a tool `bin/shrink` that:
- Minimizes the last filesystem by running `resize2fs` via `libguestfs` on the last partition.
- Shrinks the last partition by running `parted` on the last partition.
- Truncates the image to the end of the last partition.
- Rewrites the secondary partition table for `gpt` partition tables via `sgdisk`.
- Checks the partition for errors via `e2fsck` via `libguestfs`.
- Optionally checks that the filesystem has no changes by:
    - Mounting the resized image and the original image via `libguestfs`.
    - Runs `diff` across the two mounted disks.

There is currently a bash version of the script as `scripts/shrink-image.sh` which might be more portable (and also less maintained, as that script is fairly unwieldly).

### bin/shrink Usage Requirements:
- `libguestfs-tools`
- `parted`
- `sgdisk` (usually under `gdisk`)

As far as I can tell, being the group `libvirt` is not required, but it is recommended to be under if you have issues, especially if you're not running as root.

For the optional filesystem `diff`:
- `diff`
- `sudo` access
  - Even though you don't need root to mount the image via `guestmount`, you do need root to read everything in the image's filesystem.
  - This has the potential to change when we are capable of reading an image's `ext` filesystem natively.
- `user_allow_other` in `/etc/fuse.conf`.
  - `echo "user_allow_other" | sudo tee -a /etc/fuse.conf > /dev/null` will do it
  - This allows `guestmount` to use the option `allow_other`.
  - Generally, we run `bin/shrink` under a local user, but as we need to readthe mounted filesystems as root, this allows root to actually read the filesystem.
