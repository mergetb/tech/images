#!/bin/bash

set -e
set -x

# remove /etc/network/interfaces file to disable ifupdown from interfering
sudo rm -rf /etc/network/interfaces

sudo systemctl enable systemd-networkd
