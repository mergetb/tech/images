#!/bin/bash

set -e
set -x

export DEBIAN_FRONTEND=noninteractive

echo installing gnome and a few useful applications
sudo apt-get install -y \
    gdm3 \
    gnome-session \
    xterm \
    gnome-terminal \
    firefox

echo disabling screensavers and locks
cat << EOF | sudo tee /usr/share/glib-2.0/schemas/90_ubuntu-settings.gschema.override
[org.gnome.desktop.screensaver]
lock-enabled = false
[org.gnome.desktop.session]
idle-delay = 0
[org.gnome.desktop.lockdown]
disable-lock-screen = true
[org.gnome.desktop.screensaver]
idle-activation-enabled = false
[org.gnome.settings-daemon.plugins.power]
idle-dim = false
EOF

echo disabling gnome inital kickstart
cat << EOF | sudo tee /etc/gdm3/custom.conf
[daemon]
InitialSetupEnable=false
EOF

sudo glib-compile-schemas /usr/share/glib-2.0/schemas/

echo disabling gnome first-login
echo X-gnome-Autostart-enabled=false | sudo tee -a /etc/xdg/autostart/gnome-initial-setup-first-login.desktop

#echo disabling gnome update-notifier
#sudo sed -i 's/X-gnome-Autostart-Delay=60/X-gnome-Autostart-enabled=false/' /etc/xdg/autostart/update-notifier.desktop

echo disabling NetworkManager
sudo systemctl disable NetworkManager.service
sudo systemctl disable NetworkManager-wait-online.service
sudo systemctl disable NetworkManager-dispatcher.service
sudo systemctl disable network-manager.service

#sudo systemctl mask NetworkManager.service
#sudo systemctl mask NetworkManager-wait-online.service
#sudo systemctl mask NetworkManager-dispatcher.service
#sudo systemctl mask network-manager.service

echo enabling gnome
sudo systemctl enable gdm.service

echo fixing broken locales
sudo dpkg-reconfigure --frontend=noninteractive locales && sudo update-locale LANG=en_US.UTF-8

