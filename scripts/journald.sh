#!/bin/sh

set -x
set -e

# defaults to commented out "#Storage=auto"
sudo sed -i 's/^.*Storage.*$/Storage=persistent/g' /etc/systemd/journald.conf
