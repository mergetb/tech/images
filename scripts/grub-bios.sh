#!/bin/bash

set -e
set -x

## This must run before efivars provisioner as it requires efivars
## to be mounted as rw

sudo sed -i 's/GRUB_CMDLINE_LINUX=.*/GRUB_CMDLINE_LINUX="apparmor=0 root=PARTUUID=a0000000-0000-0000-0000-00000000000a rootfstype=ext4 rw net.ifnames=0 biosdevname=0 console=tty1 console=ttyS1,115200n8 earlyprintk=ttyS1"/g' /etc/default/grub

if [ "$OS" = "fedora" ]; then
	# updates the linux info, partuuid, etc.
	sudo grub2-mkconfig -o /boot/grub2/grub.cfg
else
	# updates the linux info, partuuid, etc.
	sudo update-grub2
fi
