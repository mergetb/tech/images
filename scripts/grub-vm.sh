#!/bin/bash

set -e
set -x

## This must run before efivars provisioner as it requires efivars
## to be mounted as rw

sudo sed -i "s/GRUB_CMDLINE_LINUX=.*/GRUB_CMDLINE_LINUX=\"apparmor=0 net.ifnames=0 biosdevname=0 infranet=eth0\"/g" /etc/default/grub

# updates the linux info, partuuid, etc.
sudo update-grub2
