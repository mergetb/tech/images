#!/bin/bash

set -e
set -x

## This must run before efivars provisioner as it requires efivars
## to be mounted as rw

sudo sed -i "s/GRUB_CMDLINE_LINUX=.*/GRUB_CMDLINE_LINUX=\"apparmor=0 root=PARTUUID=a0000000-0000-0000-0000-00000000000a rootfstype=ext4 rw net.ifnames=0 biosdevname=0 console=tty1 console=ttyS0,115200n8 earlyprintk=ttyS0 $CMDLINE\"/g" /etc/default/grub

if [ "$OS" = "fedora" ]; then
	sudo grub2-mkconfig -o /boot/grub2/grub.cfg
else
	# updates the linux info, partuuid, etc.
	sudo update-grub2

	# updates efi information on /boot/efi (must be rw)
	sudo grub-install /dev/sda --target=x86_64-efi --efi-directory=/boot/efi
fi

# our custom bootmgr sets ups boot off /EFI/merge, so the boot info needs to be there
sudo cp -r /boot/efi/EFI/$OS /boot/efi/EFI/merge
