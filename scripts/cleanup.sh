#!/bin/bash

set -e
set -x

sudo rm -f /etc/machine-id
sudo rm -f /var/lib/dbus/machine-id
sudo touch /etc/machine-id

sudo tee /etc/systemd/system/machine-id-init.service <<EOF
[Unit]
Description=Initialize machine id

[Service]
Type=oneshot
ExecStart=/bin/systemd-machine-id-setup

[Install]
WantedBy=multi-user.target
EOF
sudo chmod 644 /etc/systemd/system/machine-id-init.service

sudo systemctl daemon-reload
sudo systemctl enable machine-id-init.service
