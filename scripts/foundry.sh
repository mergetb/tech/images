#!/bin/bash

set -e
set -x

curl \
    -L https://gitlab.com/mergetb/tech/foundry/-/jobs/artifacts/main/raw/build/foundryc?job=make \
    -o /tmp/foundryc

cat <<EOF > /tmp/foundryc.service
[Unit]
Description=Foundry client
Documentation=https://gitlab.com/mergetb/tech/foundry

[Service]
ExecStart=/usr/local/bin/foundryc
Type=simple
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF

sudo mv /tmp/foundryc /usr/local/bin/foundryc
sudo mv /tmp/foundryc.service /lib/systemd/system/foundryc.service

sudo chmod 755 /usr/local/bin/foundryc
sudo chmod 644 /lib/systemd/system/foundryc.service

sudo systemctl enable foundryc.service
