#!/bin/bash

set -e
set -x

sudo mkdir /tmp/collect
sudo chmod a+r /tmp/collect

# rather than copy all the vmlinuz and initrd, just copy the highest
# find the largest based on semvar
kernel=`ls -1 /boot/vmlinuz-* | xargs basename -a | sort -rV | head -n1`

if [ $OS = "fedora" ]; then
    initrd=`ls -1 /boot/initramfs-*.img | xargs basename -a | sort -rV | head -n1`
else
    initrd=`ls -1 /boot/initrd.img-* | xargs basename -a | sort -rV | head -n1`
fi

sudo cp /boot/$kernel /tmp/collect/
sudo cp /boot/$initrd /tmp/collect/
cd /tmp/collect && sudo ln -s $kernel sled-kernel
cd /tmp/collect && sudo ln -s $initrd sled-initramfs
sudo chmod -R a+r /tmp/collect
