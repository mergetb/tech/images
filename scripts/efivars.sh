#!/bin/bash

set -e
set -x

sudo mv /tmp/efivars-remount.sh /bin/efivars-remount.sh
sudo chmod +x /bin/efivars-remount.sh

sudo chown root:root /tmp/efivars-remount.service
sudo mv /tmp/efivars-remount.service /etc/systemd/system/efivars-remount.service
sudo chmod 644 /etc/systemd/system/efivars-remount.service

sudo systemctl daemon-reload 
sudo systemctl enable efivars-remount.service
