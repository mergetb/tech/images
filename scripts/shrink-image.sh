#!/bin/bash

# Probably shouldn't use this over bin/shrink, but useful to have case just in case

IMAGE_FILE=$1

### save the output also to a file, if we want

# exec > >(tee -ia $IMAGE_FILE.resize-disk.log)
# exec 2> >(tee -ia $IMAGE_FILE.resize-disk.log >&2)

set -e

### helper functions

log () {
    echo '=========' $1 '========='
}

cleanup () {
    if [ ! -z "$new_tmp" ]; then
        guestunmount $new_tmp
        guestunmount $bak_tmp

        rm -rf $new_tmp $bak_tmp
    fi

    if [ "$fuse_cleanup" = true ]; then
        sudo sed -i '/^user_allow_other/d' /etc/fuse.conf
    fi
}

on_error () {
    set +e
    log "error on line $1, reverting to original disk"

    rm -rf $IMAGE_FILE
    mv $IMAGE_FILE.bak $IMAGE_FILE

    cleanup

    exit 1
}

trap 'on_error $LINENO' ERR

### backup image file
name=`basename $IMAGE_FILE`

log "backing up $IMAGE_FILE..."
echo
cp $IMAGE_FILE $IMAGE_FILE.bak

log 'before'
echo
parted $IMAGE_FILE unit b print

### get the last partition of the image
filesystems=`guestfish -a $IMAGE_FILE <<EOF
run
list-filesystems
exit
EOF
`
part=`echo "$filesystems" | tail -1 | awk '$2 ~ /ext/ {print $1}' | tr -d :`
dev=`echo $part | grep -Eo '^[^0-9]+'`
pnum=`echo $part | grep -Eo '[0-9]+$'`

if [ -z "$part" ]; then
    echo 'final partition is not ext, so not doing anything'
    rm $IMAGE_FILE.bak
    exit 0
fi

echo
log "resizing partition: $dev$pnum"

### get info about the disk and partition
part_table=`guestfish -a $IMAGE_FILE <<EOF
    run
    part-list $dev
    exit
EOF
`

ss_size=`guestfish -a $IMAGE_FILE <<EOF
    run
    blockdev-getss $dev
    exit
EOF
`

### clean and resize filesystem
tune=`guestfish -a $IMAGE_FILE <<EOF
run
mount $part /
fstrim /
umount /
e2fsck-f $part
resize2fs-M $part
tune2fs-l $part
exit
EOF
`
### get all the information needed to resize the partition
blocks=`echo "$tune" | grep 'Block count:' | awk '{print $3}'`
b_size=`echo "$tune" | grep 'Block size:' | awk '{print $3}'`

start=`echo "$part_table" | grep -EA3 'part_num:\s*'$pnum | grep 'part_start:' | awk '{print $2}'`
end=`echo $start '+' $blocks '*' $b_size '-' 1 | bc`

### acutally resize the partition
echo start: $start, blocks: $blocks, b_size: $b_size, end: $end
echo Yes | parted ---pretend-input-tty $IMAGE_FILE unit b resizepart $pnum $end

### figure out the new image size
# round up the new image size to a multiple of the sector size
new_size=`echo "$end" | awk '{print$0+(n-$0%n)%n}' n=$ss_size`

# we need to have 33 extra blocks for the partition table at the end in gpt, but not in msdos
type=`parted $IMAGE_FILE unit b print | grep 'Partition Table:' | awk '{print $3}'`

extra=33
if [ $type = "msdos" ]; then
    extra=0
fi

new_size=`echo $extra '*' $ss_size '+' $new_size | bc`

### truncate the image file, based on the new image size
log "truncate and fix: ss_size: $ss_size new_size: $new_size type: $type"

truncate --size=$new_size $IMAGE_FILE

### restore the secondary partition table in gpt
### it's stored at the end of the disk, which got deleted during the truncate
if [ $type != "msdos" ]; then
    sgdisk -e $IMAGE_FILE
fi

echo
log "after"
echo
parted $IMAGE_FILE unit b print -s
echo

### check if the partition went bad
log "checking if ok"
echo

# check the filesystem (turn off -e because we're checking the output later)
set +e
fs_check=`guestfish -a $IMAGE_FILE 2>&1 <<EOF
run
e2fsck-f $part
exit
EOF
`
set -e

if [ ! -z "$fs_check" ] ; then
    echo $fs_check
    log "$part: resize went bad"
    echo
    Error $LINENO
fi

echo "e2fsck ok..."

### mount the new image and the backup image,
### and check if there's any file differences between them

new_tmp=`mktemp -d -t resize-disk-$name-XXXXXXXXX`
bak_tmp=`mktemp -d -t resize-disk-$name-XXXXXXXXX`

# check fuse options to allow other users onto guestmount,
# because we need sudo access to check every file in the mounts
if ! grep -xq "user_allow_other" /etc/fuse.conf ; then
    fuse_cleanup=true
    echo "user_allow_other" | sudo tee -a /etc/fuse.conf > /dev/null
fi

# mount the images
guestmount -a $IMAGE_FILE -m $part --ro -o allow_other $new_tmp
guestmount -a $IMAGE_FILE.bak -m $part --ro -o allow_other $bak_tmp

# check the diff (turn off -e because we're checking the output later)
set +e

echo "checking diff..."
diff_check=`sudo diff -r -q --no-dereference $new_tmp $bak_tmp 2>&1 | grep -v '^File.*is a socket while file.*is a socket$' || true`

# clean up
guestunmount $new_tmp
guestunmount $bak_tmp

rm -rf $new_tmp $bak_tmp

new_tmp=""
bak_tmp=""

set -e

# if diff reported anything (excluding messages about sockets), something bad happened
if [ ! -z "$diff_check" ]; then
    echo "$diff_check"
    log "$part: file got corrupted"
    echo
    Error $LINENO
fi

echo "filesystems identical..."

echo

### success!
log 'resize successful! removing backup'
rm $IMAGE_FILE.bak

cleanup
