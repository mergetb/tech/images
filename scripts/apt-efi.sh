#!/bin/bash

set -e
set -x

export DEBIAN_FRONTEND=noninteractive

sudo apt-get update -y --fix-missing
sudo apt dist-upgrade -y
sudo apt install -y \
    curl \
    gnupg2 \
    ssh \
    vim-nox \
    tmux \
    tcpdump \
    ethtool \
    grub-efi \
    efibootmgr

#sudo curl https://pkg.mergetb.net/addrepo | RELEASE=bullseye bash -
#sudo apt install -y foundryc

cat <<EOF > /tmp/aa
APT::Periodic::Update-Package-Lists "0";
APT::Periodic::Unattended-Upgrade "0";
EOF

sudo rm -f /etc/discover-pkginstall.conf

sudo cp /tmp/aa /etc/apt/apt.conf.d/20auto-upgrades

sudo apt-get autoremove -y
sudo apt-get clean -y
sudo apt-get autoclean -y

cat <<EOF > /tmp/sudoers
#
# This file MUST be edited with the 'visudo' command as root.
#
# Please consider adding local content in /etc/sudoers.d/ instead of
# directly modifying this file.
#
# See the man page for details on how to write a sudoers file.
#
Defaults        env_reset
Defaults        mail_badpass
Defaults        secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

# Host alias specification

# User alias specification

# Cmnd alias specification

# User privilege specification
root    ALL=(ALL:ALL) ALL

# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) NOPASSWD:ALL

# See sudoers(5) for more information on "#include" directives:

#includedir /etc/sudoers.d
EOF

sudo cp /tmp/sudoers /etc/sudoers
