#!/bin/bash

set -e
set -x

export DEBIAN_FRONTEND=noninteractive

sudo -E apt-get update -y --fix-missing --allow-releaseinfo-change
sudo -E apt install -y git jq

sudo -E apt install -y python3-pip
sudo -E apt-get clean -y

sudo -E pip3 install --upgrade pip

#consider --prefer-binary
sudo -E pip install jupyterhub &&
	sudo -E pip install jupyterlab &&
	sudo -E pip install notebook

#also install mx
XIR_PROJECT_ID=8048911

MX_VERSION=$(curl -s https://gitlab.com/api/v4/projects/${XIR_PROJECT_ID}/releases/ | jq '.[0].tag_name' -r)
cd /tmp
git clone https://gitlab.com/mergetb/xir
cd xir
git checkout ${MX_VERSION} || true
sudo -E pip install v0.3/mx
cd ..
sudo -E rm -rf xir

