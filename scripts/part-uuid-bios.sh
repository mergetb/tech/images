#!/bin/bash

set -e
set -x

cat <<EOF > /tmp/fstab
PARTUUID=a0000000-0000-0000-0000-00000000000a /         ext4 defaults 0 1
EOF

sudo mv /tmp/fstab /etc/fstab
