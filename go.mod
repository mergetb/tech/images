module gitlab.com/mergetb/tech/images

go 1.16

require (
	github.com/diskfs/go-diskfs v1.2.0
	github.com/frankban/quicktest v1.14.3 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.4.0
	gopkg.in/yaml.v2 v2.4.0
	libguestfs.org/guestfs v0.0.0-00010101000000-000000000000
)

replace libguestfs.org/guestfs => ./.tools/libguestfs/golang/src/libguestfs.org/guestfs
