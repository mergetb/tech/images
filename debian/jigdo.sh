#!/bin/bash

# for whatever reason, jigdo is broken on fedora, so run it through a debian builder image

# the extra newlines are intentional, in case you resume the download
podman run --pull=always -v "$1":/root --rm -it registry.gitlab.com/mergetb/devops/builder/builder:latest jigdo-lite "$2" <<EOF

http://ftp.us.debian.org/debian/










EOF
