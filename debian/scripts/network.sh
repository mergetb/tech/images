#!/bin/bash

set -e
set -x

# remove /etc/network/interfaces file to disable ifupdown from interfering
sudo rm -rf /etc/network/interfaces
# remove the two ifupdown services from starting
sudo systemctl disable ifupdown-pre.service
sudo systemctl disable ifupdown-wait-online.service

sudo systemctl enable systemd-networkd
sudo systemctl enable systemd-resolved
