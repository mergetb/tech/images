#!/bin/bash

set -e
set -x

export DEBIAN_FRONTEND=noninteractive

###
# bullseye systemd version (247.3-7 as of bullseye 11.3.0) has a networkd bug that is problematic:
# - https://github.com/systemd/systemd/issues/18108
# - https://github.com/systemd/systemd/issues/20232
#
# foundry uses systemd-networkd to configure networks on boot, and does occasionally hit this bug.
# The bug is non-deterministic and the issues linked suggest it's more likely to occur when a lot
# of activity is happening; for us, this means either nodes with many interfaces and/or many
# routes

# bullseye-backports has a newer version (250.4.1~po11+1 as of 04-28-2022) that
# likely resolves the issue, so install here until no longer needed

sudo tee -a /etc/apt/sources.list.d/bullseye-backports.list <<EOF
deb http://deb.debian.org/debian/ bullseye-backports main contrib non-free
deb-src http://deb.debian.org/debian/ bullseye-backports main contrib non-free
EOF

sudo -E apt -y update
sudo -E apt -y -t bullseye-backports install systemd

sudo -E apt-get autoremove -y
sudo -E apt-get clean -y
sudo -E apt-get autoclean -y
